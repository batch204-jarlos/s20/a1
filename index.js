// console.log("hello!");

let number = Number(prompt("Enter a number"));

for (let x = number; x >= 0; x--) {


	if (x <= 50) {
		console.log("The current value is 50. Terminating the loop");
		break;
	}

	if (x % 10 == 0) {
		console.log("The number is divisible by 10. Skipping the number");
		continue;
	}

	if (x % 5 == 0) {
		console.log(x);
		continue;
	}

}

let word = "supercalifragilisticexpialidocious";
let consonant = "";

for (i = 0; i < word.length; i++) {

	if (word[i] === "a" || word[i] === "e" || word[i] === "i" || word[i] === "o" || word[i] === "u" ) {
		continue;
	}	

	else {
		consonant += word[i]
	}
}

console.log(word);
console.log(consonant);